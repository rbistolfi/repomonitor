# coding: utf8

"""Provides a base class for handlers of inotify events

"""


__all__ = ["INotifyEventHandler"]


import os, sys
from twisted.internet import inotify, reactor
from twisted.python import filepath, log
from twisted.application import internet, service


class INotifyEventHandler(object):
    """A class handling INotify events.
    Handlers are implemented as methods named with "on_" + the human readable
    form of the mask, as defined in t.i.inotify

    """
    def __init__(self, root_path):
        """Create an INotify object and expose the path to the root.

        """
        self.root = root_path
        self.inotify = inotify.INotify()

    def dispatch(self, ignored, filepath, mask):
        """Dispatch an event to its handler if exists

        """
        for m in inotify.humanReadableMask(mask):
            handler = "on_" + m
            method = getattr(self, handler, None)
            if method:
                method(filepath, mask)

    def register(self):
        """Start reading events and add a watch

        """
        self.inotify.startReading()
        self.inotify.watch(filepath.FilePath(self.root), callbacks=[self.dispatch])

    def unregister(self):
        """Stop reading and remove watchers

        """
        self.inotify.connectionLost("Shutting down")


