#!/usr/bin/env python

from distutils.core import setup

setup(name='repowatcher',
      version='0.1a',
      description='Twisted-INotify daemon for reacting to repository fs changes',
      author='Rodrigo Bistolfi',
      author_email='moc.liamg@iflotsibr'[::-1],
      packages=["repomonitor"],
      data_files=[("/usr/lib/repomonitor/", ["service.tac"]),
          ("/etc/rc.d", ["rc.repomonitor"])],
      )
